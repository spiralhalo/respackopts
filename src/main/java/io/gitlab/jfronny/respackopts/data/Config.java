package io.gitlab.jfronny.respackopts.data;

import com.google.gson.JsonObject;

public class Config {
    public JsonObject bools;
    public JsonObject doubles;
    public JsonObject strings;
}
