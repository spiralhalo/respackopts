package io.gitlab.jfronny.respackopts.abstractions;

import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigCategory;

public class JfConfigCategoryPrimary implements JfConfigCategory {
    ConfigCategory base;
    public JfConfigCategoryPrimary(ConfigCategory category) {
        base = category;
    }

    @Override
    public JfConfigCategory addEntry(AbstractConfigListEntry var1) {
        base.addEntry(var1);
        return this;
    }
}
