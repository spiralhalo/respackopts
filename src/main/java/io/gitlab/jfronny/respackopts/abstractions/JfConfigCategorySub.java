package io.gitlab.jfronny.respackopts.abstractions;

import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.impl.builders.SubCategoryBuilder;

public class JfConfigCategorySub implements JfConfigCategory {
    SubCategoryBuilder sc;
    public JfConfigCategorySub(SubCategoryBuilder sc) {
        this.sc = sc;
    }
    @Override
    public JfConfigCategory addEntry(AbstractConfigListEntry var1) {
        sc.add(var1);
        return this;
    }
}
