package io.gitlab.jfronny.respackopts.abstractions;

import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;

public interface JfConfigCategory {
    JfConfigCategory addEntry(AbstractConfigListEntry var1);
}
