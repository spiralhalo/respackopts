package io.gitlab.jfronny.respackopts;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.gitlab.jfronny.respackopts.conditions.SyntaxError;
import io.gitlab.jfronny.respackopts.data.Config;
import io.gitlab.jfronny.respackopts.data.Respackmeta;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.loader.api.FabricLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Environment(EnvType.CLIENT)
public class Respackopts implements ClientModInitializer {
    public static Map<String, Map<String, Boolean>> boolVals;
    public static Map<String, Map<String, Double>> numVals;
    public static Map<String, Map<String, String>> strVals;
    public static Map<String, Map<String, Set<String>>> enumKeys;
    public static Gson g = new Gson();
    public static GuiFactory factory = new GuiFactory();
    public static final Integer metaVersion = 1;
    public static Map<String, Respackmeta> resPackMetas = new HashMap<>();
    public static final String ID = "respackopts";
    static final Path p = FabricLoader.getInstance().getConfigDir().resolve("respackopts");
    public static final Set<Runnable> saveActions = new HashSet<>();
    public static final String fileExtension = ".rpo";
    public static boolean forceRespackReload = false;
    public static final Logger logger = LogManager.getFormatterLogger(ID);
    @Override
    public void onInitializeClient() {
        try {
            Files.createDirectories(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (boolVals == null)
            boolVals = new HashMap<>();
        if (numVals == null)
            numVals = new HashMap<>();
        if (strVals == null)
            strVals = new HashMap<>();
        if (enumKeys == null)
            enumKeys = new HashMap<>();
        if (FabricLoader.getInstance().isDevelopmentEnvironment())
            saveActions.add(() -> logger.info("Save"));
    }

    public static void save() {
        for (String s : resPackMetas.keySet()) {
            s = resPackMetas.get(s).id;
            Config cfg = new Config();
            if (boolVals.containsKey(s))
            {
                cfg.bools = new JsonObject();
                for (Map.Entry<String, Boolean> entry : boolVals.get(s).entrySet()) {
                    cfg.bools.addProperty(entry.getKey(), entry.getValue());
                }
            }
            if (numVals.containsKey(s))
            {
                cfg.doubles = new JsonObject();
                for (Map.Entry<String, Double> entry : numVals.get(s).entrySet()) {
                    cfg.doubles.addProperty(entry.getKey(), entry.getValue());
                }
            }
            if (strVals.containsKey(s))
            {
                cfg.strings = new JsonObject();
                for (Map.Entry<String, String> entry : strVals.get(s).entrySet()) {
                    cfg.strings.addProperty(entry.getKey(), entry.getValue());
                }
            }
            try {
                Writer writer = Files.newBufferedWriter(p.resolve(s + ".json"));
                g.toJson(cfg, writer);
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (Runnable action : saveActions) {
            action.run();
        }
    }

    public static void load(String id) {
        Path q = p.resolve(id + ".json");
        if (Files.exists(q)) {
            try {
                Reader reader = Files.newBufferedReader(q);
                Config c = g.fromJson(reader, Config.class);
                reader.close();
                if (c.bools != null) {
                    boolVals.get(id).forEach((i, val) -> {
                        if (c.bools.has(i)
                                && c.bools.get(i).isJsonPrimitive()
                                && c.bools.get(i).getAsJsonPrimitive().isBoolean()) {
                            boolVals.get(id).put(i, c.bools.get(i).getAsBoolean());
                        } else if (FabricLoader.getInstance().isDevelopmentEnvironment())
                            Respackopts.logger.error("Could not find bool " + i + " in " + id);
                    });
                }
                if (c.doubles != null) {
                    numVals.get(id).forEach((i, val) -> {
                        if (c.doubles.has(i)
                                && c.doubles.get(i).isJsonPrimitive()
                                && c.doubles.get(i).getAsJsonPrimitive().isNumber()) {
                            numVals.get(id).put(i, c.doubles.get(i).getAsNumber().doubleValue());
                        } else if (FabricLoader.getInstance().isDevelopmentEnvironment())
                            Respackopts.logger.error("Could not find num " + i + " in " + id);
                    });
                }
                if (c.strings != null) {
                    strVals.get(id).forEach((i, val) -> {
                        if (c.strings.has(i)
                                && c.strings.get(i).isJsonPrimitive()
                                && c.strings.get(i).getAsJsonPrimitive().isString()) {
                            strVals.get(id).put(i, c.strings.get(i).getAsString());
                        } else if (FabricLoader.getInstance().isDevelopmentEnvironment())
                            Respackopts.logger.error("Could not find str " + i + " in " + id);
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean matchStringCondition(String condition) throws SyntaxError {
        if (condition == null) {
            throw new SyntaxError("Condition must not be null");
        }
        if (!condition.contains(":")) {
            throw new SyntaxError("You must include you resource pack ID in conditions (format: pack:some.key)");
        }
        AtomicBoolean found = new AtomicBoolean(false);
        AtomicBoolean output = new AtomicBoolean(false);
        Respackopts.resPackMetas.forEach((r, v) -> {
            String sourcePack = condition.split(":")[0];
            String name = condition.substring(condition.indexOf(':') + 1);
            if (Objects.equals(v.id, sourcePack)) {
                found.set(true);
                output.set(Respackopts.boolVals.get(sourcePack).get(name));
            }
        });
        if (!found.get()) {
            throw new SyntaxError("Could not find pack with specified ID");
        }
        return output.get();
    }
}
