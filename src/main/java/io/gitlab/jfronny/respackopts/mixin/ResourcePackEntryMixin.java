package io.gitlab.jfronny.respackopts.mixin;

import com.mojang.blaze3d.systems.RenderSystem;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.Respackmeta;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.pack.PackListWidget;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;
import net.minecraft.client.gui.widget.AlwaysSelectedEntryListWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
@Mixin(PackListWidget.ResourcePackEntry.class)
public abstract class ResourcePackEntryMixin extends AlwaysSelectedEntryListWidget.Entry<PackListWidget.ResourcePackEntry> {
    @Shadow private PackListWidget widget;

    @Shadow protected abstract boolean isSelectable();

    @Shadow @Final private ResourcePackOrganizer.Pack pack;
    boolean respackopts$selected;

    @Inject(at = @At("TAIL"), method = "render(Lnet/minecraft/client/util/math/MatrixStack;IIIIIIIZF)V")
    private void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta, CallbackInfo info) {
        String k = pack.getDisplayName().asString();
        if (this.isSelectable() && Respackopts.resPackMetas.containsKey(k)) {
            int d = mouseX - x;
            d = widget.getRowWidth() - d;
            int e = mouseY - y;
            respackopts$selected = d <= 32 && d >= 12 && e <= entryHeight / 2 + 10 && e >= entryHeight / 2 - 10;
            respackopts$renderButton(matrices, respackopts$selected, new Identifier("modmenu", "textures/gui/configure_button.png"), x + entryWidth - 30, y + entryHeight / 2 - 10, 20, 20, 0, 0, 32, 64);
        }
    }

    @Inject(at = @At("RETURN"), method = "mouseClicked(DDI)Z", cancellable = true)
    public void mouseClicked(double mouseX, double mouseY, int button, CallbackInfoReturnable<Boolean> info) {
        if (!info.getReturnValue()) {
            if (this.isSelectable()) {
                String k = pack.getDisplayName().asString();
                if (Respackopts.resPackMetas.containsKey(k) && respackopts$selected) {
                    info.setReturnValue(true);
                    Respackmeta meta = Respackopts.resPackMetas.get(k);
                    MinecraftClient c = MinecraftClient.getInstance();
                    c.openScreen(Respackopts.factory.buildGui(meta.conf, meta.id, c.currentScreen, () -> Respackopts.forceRespackReload = true));
                }
            }
        }
    }

    private void respackopts$renderButton(MatrixStack matrices, boolean hovered, Identifier texture, int x, int y, int width, int height, int u, int v, int uWidth, int vHeight) {
        MinecraftClient client = MinecraftClient.getInstance();
        client.getTextureManager().bindTexture(texture);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.disableDepthTest();
        int adjustedV = v;
        if (hovered) {
            adjustedV += height;
        }
        DrawableHelper.drawTexture(matrices, x, y, u, (float)adjustedV, width, height, uWidth, vHeight);
        RenderSystem.enableDepthTest();
    }
}
