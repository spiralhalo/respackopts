package io.gitlab.jfronny.respackopts.mixin.conditions;

import io.gitlab.jfronny.respackopts.conditions.ResourcePackFilter;
import net.minecraft.resource.AbstractFileResourcePack;
import net.minecraft.resource.ResourceNotFoundException;
import net.minecraft.resource.ResourceType;
import net.minecraft.resource.ZipResourcePack;
import net.minecraft.util.Identifier;
import org.apache.commons.io.input.NullInputStream;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Set;
import java.util.function.Predicate;

@Mixin(ZipResourcePack.class)
public abstract class ZipResourcePackMixin extends AbstractFileResourcePack {
    public ZipResourcePackMixin(File base) {
        super(base);
    }

    @Shadow
    protected abstract boolean containsFile(String name);

    @Shadow public abstract Set<String> getNamespaces(ResourceType type);

    @Shadow protected abstract InputStream openFile(String name) throws IOException;

    ResourcePackFilter filter = new ResourcePackFilter(this::containsFile, (s) -> {
        try {
            return openFile(s);
        } catch (IOException e) {
            e.printStackTrace();
            return new NullInputStream(0);
        }
    });
    @Inject(at = @At("HEAD"), method = "openFile(Ljava/lang/String;)Ljava/io/InputStream;", cancellable = true)
    protected void openFile(String name, CallbackInfoReturnable<InputStream> info) throws IOException {
        if (!filter.fileVisible(name)) {
            throw new ResourceNotFoundException(this.base, name);
        }
    }

    @Inject(at = @At("TAIL"), method = "containsFile(Ljava/lang/String;)Z", cancellable = true)
    protected void containsFile(String name, CallbackInfoReturnable<Boolean> info) {
        info.setReturnValue(info.getReturnValueZ() && filter.fileVisible(name));
    }

    @Inject(at = @At("TAIL"), method = "findResources(Lnet/minecraft/resource/ResourceType;Ljava/lang/String;Ljava/lang/String;ILjava/util/function/Predicate;)Ljava/util/Collection;")
    private void findResources(ResourceType type, String namespace, String prefix, int maxDepth, Predicate<String> pathFilter, CallbackInfoReturnable<Collection<Identifier>> info) {
        info.getReturnValue().removeIf(s -> !filter.fileVisible(s.getPath()));
    }
}
