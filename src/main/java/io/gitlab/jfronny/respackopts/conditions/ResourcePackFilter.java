package io.gitlab.jfronny.respackopts.conditions;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.RpoResourceEntry;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.Function;
import java.util.function.Predicate;

public class ResourcePackFilter {
    Predicate<String> containsFileBase;
    Function<String, InputStream> openFileBase;
    public ResourcePackFilter(Predicate<String> containsFileBase, Function<String, InputStream> openFileBase) {
        this.containsFileBase = containsFileBase;
        this.openFileBase = openFileBase;
    }

    public boolean fileVisible(String name) {
        if (name.endsWith(Respackopts.fileExtension))
            return true;
        if (!containsFileBase.test(name + Respackopts.fileExtension))
            return true;
        try (InputStream stream = openFileBase.apply(name + Respackopts.fileExtension); Reader w = new InputStreamReader(stream)) {
            RpoResourceEntry entry = Respackopts.g.fromJson(w, RpoResourceEntry.class);
            if (entry.conditions != null) {
                for (String condition : entry.conditions) {
                    if (!Respackopts.matchStringCondition(condition))
                        return false;
                }
                return true;
            }
            else {
                Respackopts.logger.error("Conditions null for " + name);
                return true;
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
            return true;
        }
    }
}
