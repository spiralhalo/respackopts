package io.gitlab.jfronny.respackopts.conditions;

public class SyntaxError extends Exception {
    public SyntaxError(String message) {
        super(message);
    }
}
