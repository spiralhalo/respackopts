package io.gitlab.jfronny.respackopts;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import io.gitlab.jfronny.respackopts.abstractions.JfConfigCategory;
import io.gitlab.jfronny.respackopts.abstractions.JfConfigCategoryPrimary;
import io.gitlab.jfronny.respackopts.abstractions.JfConfigCategorySub;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.gui.entries.DropdownBoxEntry;
import me.shedaniel.clothconfig2.impl.builders.DropdownMenuBuilder;
import me.shedaniel.clothconfig2.impl.builders.SubCategoryBuilder;
import net.minecraft.client.gui.screen.FatalErrorScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.*;
import net.minecraft.util.Language;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class GuiFactory {
    public void buildCategory(JsonObject source, String screenId, JfConfigCategory config, ConfigEntryBuilder entryBuilder, String namePrefix) {
        if (!Respackopts.boolVals.containsKey(screenId))
            Respackopts.boolVals.put(screenId, new HashMap<>());
        if (!Respackopts.numVals.containsKey(screenId))
            Respackopts.numVals.put(screenId, new HashMap<>());
        if (!Respackopts.strVals.containsKey(screenId))
            Respackopts.strVals.put(screenId, new HashMap<>());
        if (!Respackopts.enumKeys.containsKey(screenId))
            Respackopts.enumKeys.put(screenId, new HashMap<>());
        String b = "respackopts.field." + screenId;
        for (Map.Entry<String, JsonElement> entry : source.entrySet()) {
            String n = ("".equals(namePrefix) ? "" : namePrefix + ".") + entry.getKey();
            JsonElement e = entry.getValue();
            if (e.isJsonPrimitive()) {
                JsonPrimitive p = e.getAsJsonPrimitive();
                if (p.isBoolean()) {
                    boolean defaultValue = p.getAsBoolean();
                    boolean currentValue = defaultValue;
                    if (Respackopts.boolVals.get(screenId).containsKey(n))
                        currentValue = Respackopts.boolVals.get(screenId).get(n);
                    else
                        Respackopts.boolVals.get(screenId).put(n, defaultValue);
                    config.addEntry(entryBuilder.startBooleanToggle(getText(n, b), currentValue)
                            .setDefaultValue(defaultValue)
                            .setSaveConsumer(v -> Respackopts.boolVals.get(screenId).put(n, v))
                            .setTooltipSupplier(() -> getTooltip(n, screenId))
                            .build());
                }
                else if (p.isNumber()) {
                    double defaultValue = p.getAsDouble();
                    Double currentValue = defaultValue;
                    if (Respackopts.numVals.get(screenId).containsKey(n))
                        currentValue = Respackopts.numVals.get(screenId).get(n);
                    else
                        Respackopts.numVals.get(screenId).put(n, defaultValue);
                    config.addEntry(entryBuilder.startDoubleField(getText(n, b), currentValue)
                            .setDefaultValue(defaultValue)
                            .setSaveConsumer(v -> Respackopts.numVals.get(screenId).put(n, v))
                            .setTooltipSupplier(() -> getTooltip(n, screenId))
                            .build());
                }
                else if (p.isString()) {
                    String defaultValue = p.getAsString();
                    String currentValue = defaultValue;
                    if (Respackopts.strVals.get(screenId).containsKey(n))
                        currentValue = Respackopts.strVals.get(screenId).get(n);
                    else
                        Respackopts.strVals.get(screenId).put(n, defaultValue);
                    config.addEntry(entryBuilder.startStrField(getText(n, b), currentValue)
                            .setDefaultValue(defaultValue)
                            .setSaveConsumer(v -> Respackopts.strVals.get(screenId).put(n, v))
                            .setTooltipSupplier(() -> getTooltip(n, screenId))
                            .build());
                }
            }
            else if (e.isJsonArray()) {
                Set<String> ev = Respackopts.enumKeys.get(screenId).get(n);
                Double c = Respackopts.numVals.get(screenId).get(n);
                String sel = "";
                String def = "";
                int i = 0;
                for (String s1 : ev) {
                    if (i == 0) def = s1;
                    if (c.intValue() == i) sel = s1;
                    i++;
                }
                config.addEntry(entryBuilder.startDropdownMenu(getText(n, b), (DropdownBoxEntry.SelectionTopCellElement) DropdownMenuBuilder.TopCellElementBuilder.of(sel, (s) -> s, (s) -> new LiteralText(s)), new DropdownBoxEntry.DefaultSelectionCellCreator())
                        .setSuggestionMode(false)
                        .setDefaultValue(def)
                        .setSelections(() -> ev.iterator())
                        .setSaveConsumer(v -> {
                            int j = 0;
                            for (String s1 : ev) {
                                if (s1.equals(v))
                                    Respackopts.numVals.get(screenId).put(n, (double) j);
                                j++;
                            }
                        })
                        .setTooltipSupplier(() -> getTooltip(n, screenId))
                        .build());
            }
            else if (e.isJsonNull()) {
                Respackopts.logger.error("Config definition contains null, skipping that entry");
            }
            else if (e.isJsonObject()) {
                JsonObject data = e.getAsJsonObject();
                //Slider
                if (data.entrySet().stream().allMatch(s -> "default".equals(s.getKey()) || "min".equals(s.getKey()) || "max".equals(s.getKey())))
                {
                    JsonElement def = data.get("default");
                    JsonElement min = data.get("min");
                    JsonElement max = data.get("max");
                    if (def.isJsonPrimitive() && def.getAsJsonPrimitive().isNumber()
                            && min.isJsonPrimitive() && min.getAsJsonPrimitive().isNumber()
                            && max.isJsonPrimitive() && max.getAsJsonPrimitive().isNumber()) {
                        Double defV = def.getAsNumber().doubleValue();
                        Double minV = min.getAsNumber().doubleValue();
                        Double maxV = max.getAsNumber().doubleValue();
                        if (defV < minV || defV > maxV) {
                            Respackopts.logger.error("default value out of range");
                        }
                        else if (isWhole(defV) && isWhole(minV) && isWhole(maxV)) {
                            Double currentValue = defV;
                            if (Respackopts.numVals.get(screenId).containsKey(n))
                                currentValue = Respackopts.numVals.get(screenId).get(n);
                            else Respackopts.numVals.get(screenId).put(n, defV);
                            if (currentValue < minV)
                                currentValue = minV;
                            if (currentValue > maxV)
                                currentValue = maxV;
                            config.addEntry(entryBuilder.startIntSlider(getText(n, b),
                                    currentValue.intValue(), minV.intValue(), maxV.intValue())
                                    .setDefaultValue(defV.intValue())
                                    .setSaveConsumer(v -> Respackopts.numVals.get(screenId).put(n, v.doubleValue()))
                                    .setTooltipSupplier(() -> getTooltip(n, screenId))
                                    .build());
                        }
                        else {
                            Respackopts.logger.error("Expected whole number in slider definition");
                        }
                    }
                    else {
                        Respackopts.logger.error("Expected numeric values in slider definition");
                    }
                    continue;
                }
                //Normal object
                SubCategoryBuilder sc = entryBuilder.startSubCategory(getText(n, "respackopts.title." + screenId));
                buildCategory(data, screenId, new JfConfigCategorySub(sc), entryBuilder, n);
                sc.setTooltipSupplier(() -> getTooltip(n, screenId));
                config.addEntry(sc.build());
            }
            else {
                Respackopts.logger.error("Unsupported non-primitive datatype");
            }
        }
    }

    private Optional<Text[]> getTooltip(String key, String screenId) {
        String k = "respackopts.tooltip." + screenId + "." + key;
        if (Language.getInstance().hasTranslation(k)) {
            Text[] res = new Text[1];
            res[0] = new TranslatableText(k);
            return Optional.of(res);
        }
        else
            return Optional.empty();
    }

    private boolean isWhole(double v) {
        return v == Math.floor(v) && !Double.isInfinite(v);
    }

    public Screen buildGui(JsonObject source, String resourcepackid, Screen parent, Runnable onSave) {
        try {
            ConfigBuilder builder;
            builder = ConfigBuilder.create()
                    .setParentScreen(parent)
                    .setTitle(getText(resourcepackid, "respackopts.title"));
            ConfigEntryBuilder entryBuilder = builder.entryBuilder();
            builder.setSavingRunnable(() -> {
                Respackopts.save();
                onSave.run();
            });
            ConfigCategory config = builder.getOrCreateCategory(getText(resourcepackid, "respackopts.title"));
            buildCategory(source, resourcepackid, new JfConfigCategoryPrimary(config), entryBuilder, "");
            return builder.build();
        }
        catch (Throwable t) {
            t.printStackTrace();
            return new FatalErrorScreen(new TranslatableText("respackopts.loadFailed"), new TranslatableText("respackopts.loadError"));
        }
    }

    private Text getText(String key, String idPrefix) {
        String k = idPrefix + "." + key;
        if (Language.getInstance().hasTranslation(k)) return new TranslatableText(k);
        else return new LiteralText(key);
    }
}
