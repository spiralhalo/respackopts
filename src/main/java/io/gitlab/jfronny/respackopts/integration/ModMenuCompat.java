package io.gitlab.jfronny.respackopts.integration;

import io.github.prospector.modmenu.api.ConfigScreenFactory;
import io.github.prospector.modmenu.api.ModMenuApi;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.abstractions.JfConfigCategoryPrimary;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.FatalErrorScreen;
import net.minecraft.text.TranslatableText;

public class ModMenuCompat implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> {
            try {
                ConfigBuilder builder;
                builder = ConfigBuilder.create()
                        .setParentScreen(parent)
                        .setTitle(new TranslatableText("respackopts.mainconfig"));
                ConfigEntryBuilder entryBuilder = builder.entryBuilder();
                builder.setSavingRunnable(() -> {
                    Respackopts.save();
                    MinecraftClient.getInstance().reloadResources();
                });
                Respackopts.resPackMetas.forEach((s, v) -> {
                    ConfigCategory config = builder.getOrCreateCategory(new TranslatableText("respackopts.title." + v.id));
                    Respackopts.factory.buildCategory(v.conf, v.id, new JfConfigCategoryPrimary(config), entryBuilder, "");
                });
                return builder.build();
            }
            catch (Throwable t) {
                t.printStackTrace();
                return new FatalErrorScreen(new TranslatableText("respackopts.loadFailed"), new TranslatableText("respackopts.loadError"));
            }
        };
    }
}
