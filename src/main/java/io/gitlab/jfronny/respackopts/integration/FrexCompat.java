package io.gitlab.jfronny.respackopts.integration;

import grondag.frex.FrexInitializer;
import grondag.frex.api.config.ShaderConfig;
import io.gitlab.jfronny.respackopts.Respackopts;
import net.minecraft.util.Identifier;

import java.util.concurrent.atomic.AtomicInteger;

public class FrexCompat implements FrexInitializer {
    boolean initial = true;
    @Override
    public void onInitalizeFrex() {
        ShaderConfig.registerShaderConfigSupplier(new Identifier(Respackopts.ID, "config_supplier"), () -> {
            StringBuilder sb = new StringBuilder();
            Respackopts.numVals.forEach((s, v) -> v.forEach((s1, v1) -> {
                sb.append("\n");
                sb.append("#define ");
                sb.append(s.replace('.', '_'));
                sb.append("_");
                sb.append(s1.replace('.', '_'));
                sb.append(" ");
                String tmp = v1.toString();
                if (tmp.endsWith(".0"))
                    tmp = tmp.substring(0, tmp.length() - 2);
                sb.append(tmp);
            }));
            Respackopts.boolVals.forEach((s, v) -> v.forEach((s1, v1) -> {
                if (v1) {
                    sb.append("\n");
                    sb.append("#define ");
                    sb.append(s.replace('.', '_'));
                    sb.append("_");
                    sb.append(s1.replace('.', '_'));
                }
            }));
            Respackopts.enumKeys.forEach((s, v) -> v.forEach((s1, v1) -> {
                AtomicInteger i = new AtomicInteger(0);
                v1.forEach((s2) -> {
                    sb.append("\n");
                    sb.append("#define ");
                    sb.append(s.replace('.', '_'));
                    sb.append("_");
                    sb.append(s1.replace('.', '_'));
                    sb.append("_");
                    sb.append(s2.replace('.', '_'));
                    sb.append(" ");
                    sb.append(i.getAndIncrement());
                });
            }));
            sb.append("\n#define respackopts_loaded");
            return sb.toString();
        });
        Respackopts.logger.info("enabled frex/canvas support");
        Respackopts.saveActions.add(() -> {
            try {
                if (!initial)
                    ShaderConfig.invalidateShaderConfig();
                initial = false;
            }
            catch (Throwable e) {
                e.printStackTrace();
            }
        });
    }
}
