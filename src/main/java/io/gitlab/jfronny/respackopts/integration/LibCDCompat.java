package io.gitlab.jfronny.respackopts.integration;

import io.github.cottonmc.libcd.api.CDSyntaxError;
import io.github.cottonmc.libcd.api.condition.ConditionManager;
import io.github.cottonmc.libcd.api.init.ConditionInitializer;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.conditions.SyntaxError;
import net.minecraft.util.Identifier;

public class LibCDCompat implements ConditionInitializer {
    @Override
    public void initConditions(ConditionManager conditionManager) {
        conditionManager.registerCondition(new Identifier(Respackopts.ID, "cfg"), q -> {
            if (!(q instanceof String)) {
                throw new CDSyntaxError("Expected string");
            }
            String s = (String) q;
            try {
                return Respackopts.matchStringCondition(s);
            } catch (SyntaxError syntaxError) {
                throw new CDSyntaxError(syntaxError.getMessage());
            }
        });
    }
}
