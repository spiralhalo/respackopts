# Respackopts
Respackopts provides resource packs with config menus\
By default it integrates with frex (canvas shaders, currently requires the latest development release) and libcd (use this as reference)\
An example for the frex/canvas integration can be found [here](https://gitlab.com/JFronny/respackopts/-/tree/master/run/resourcepacks/lumi)

# Using Respackopts
## Users
You will just need to install Respackopts. A menu button will appear besides all supported resourcepacks.\
You can get the latest debug build [here](https://gitlab.com/JFronny/respackopts/-/jobs/artifacts/master/raw/latest.jar?job=build_test) and the latest stable build [here](https://modrinth.com/mod/respackopts)
## Resource pack authors
You will need to define a respackopts conf as seen [here](https://gitlab.com/JFronny/respackopts/-/blob/master/run/resourcepacks/lumi/assets/respackopts/conf.json)

This config may include:
- boolean values, expressed as Json bools
- number values, expressed as Json numbers
- slider-based numeric values, expressed as a json object containing only the fields `default`, `min` and `max` and only whole numbers
- string values (not accessible from default integrations), expressed as Json strings
- enums, expressed as Json arrays containing simple strings
- subcategories expressed as json objects. These must meet the same constraints as the root config

You can also provide translations/more complex names for your resource pack's configs.\
The following things can be changed:
- Entry names: `respackopts.field.{packId}.{entryId}`
- Category titles: `respackopts.title.{packId}`
- Tooltips: `respackopts.tooltip.{packId}.{entryId}`

In subcategories, you can use `{packId}.{categoryId}` instead of `{packId}`

Please note that translations require your resource pack to be loaded.

The version property defines the format version. It will be incremented if changes to the layout of the config are made\
I will likely attempt to support older versions if I decide to make an incompatible change.\
To use the information from the config you will need to use and integration module/plugin.\
For the ones included by default:
### Canvas
Respackopts defines a config supplier, you can include it with `#include respackopts:config_supplier`\
You can always check whether it was properly loaded by checking if `respackopts_loaded` is defined.\
Values can be accessed with `{packId}_{entryId}` for basic elements.\
Enum values can be accessed with `{packId}_{entryId}_{enumKey}`.\
Examples of using these are available [here](https://gitlab.com/JFronny/respackopts/-/tree/master/run/resourcepacks/lumi)
### LibCD
The LibCD integration defines a condition named `respackopts:cfg` that takes a string describing the location of a bool as `{packId}:{entryId}`

There have been issues with LibCD in development, so it might be smart not to use it.
### Conditional resources
Respackopts allows creating conditional resources by creating a file named `{targetFile}.rpo`.\
This file is a json file that contains an array named "conditions". The resource will be ignored if any of the conditions are not met.\
This allows (for example) overrides for textures to only be loaded if the user enables them through the config.\
As an example can be seen [here](https://gitlab.com/JFronny/respackopts/-/tree/master/run/resourcepacks/lumi/assets/minecraft/lang)
## Mod developers
All data is available in static HashMaps in `io.gitlab.jfronny.respackopts.Respackopts`.\
To save information, call `Respackopts.save()`, `Respackopts.load()` to load.
`boolVals`, `numVals`, `strVals` and `enumKeys` use the resource pack ID as their first key,
then the property name. After that you get the value. Enums are expressed as numbers with enum key names in enumKeys.\
If you need code to be ran after changes are made to the state, add an action to `saveActions`.\
This is currently used for the Canvas/FREX integration, which uses it to generate shader code.\
The config GUI is currently generated using cloth config. The config icon ís loaded from ModMenu. It will not display if that is not loaded.