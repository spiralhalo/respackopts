# Lumi Lights
[Lumi Lights](https://github.com/spiralhalo/LumiLights) by [spiralhalo](https://github.com/spiralhalo/LumiLights) is a shader for canvas.\
This modified version uses respackopts for configuration instead of forcing the user to edit glsl